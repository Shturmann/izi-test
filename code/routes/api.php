<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function (){
    Route::post('user/profile','UserController@profile');
    Route::post('user/ban','UserController@ban')->middleware('is.admin');

    Route::prefix('notes')->group(function () {
        Route::get('/', 'NoteController@index');
        Route::post('store', 'NoteController@store');
        Route::post('delete', 'NoteController@destroy');
    });
});
