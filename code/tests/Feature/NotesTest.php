<?php

namespace Tests\Feature;

use App\Note;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\WithStubUser;

class NotesTest extends TestCase
{
    use DatabaseTransactions, WithStubUser;

    public function test_index_authentication()
    {
        $this->assertAuthenticationRequired('/notes');
        $this->assertAuthenticationRequired('/notes/store');
        $this->assertAuthenticationRequired('/notes/delete');
    }

    public function testIndex()
    {
        $user = $this->createStubUser();
        $response = $this->actingAs($user)->get('/notes');

        $response->assertStatus(200);;
    }

    public function testAuthUserCanStoreNewNote()
    {
        $this->actingAs($this->createStubUser());

        $this->get('/notes/store', ['text' => 'test'])
            ->assertStatus(200);
    }

    public function testStoreInvalidNote()
    {
        $this->actingAs($this->createStubUser());

        $this->postJson('/notes/store', ['text' => ''])
            ->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['text']]);
    }

    public function testAuthUserCanEditNote()
    {
        $note = $this->createNote();

        $this->post("/notes/edit", $note->id)
            ->assertStatus(200);

        $this->put("/notes/{$note->id}", ['text' => 'updated text']);
    }

    public function testAuthUserCanDeleteNote()
    {
        $note = $this->createNote();

        $this->post("/notes/delete", $note->id)
            ->assertRedirect('/notes')
            ->assertSessionHas('deleted', $note->id);
    }

    private function createNote($authenticated = true)
    {
        $note = factory(Note::class)->create();

        if ($authenticated) {
            $this->actingAs($this->createStubUser());
        }

        return $note;
    }
}
