import VueRouter from 'vue-router';


let routes = [
    {
        path: '/',
        name: 'Notes',
        component: require('./views/notes').default
    },
    {
        path: '/profile',
        name: 'Profile',
        component: require('./views/profile').default,
        props: true
    }
];


export default new VueRouter({
    base: '/notes',
    mode: 'history',
    routes,
    linkActiveClass: 'active'
});
