<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\NotesCollection;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NoteController extends Controller
{
    private $note;

    public function __construct(Note $note)
    {
        $this->note = $note;
    }

    /**
     * Display a listing of the resource.
     *
     * @return NotesCollection
     */
    public function index()
    {
        $notes = $this->note->select('notes.*', 'users.name', 'users.email')
                    ->join('users', 'notes.user_id', '=', 'users.id')
                    ->latest('id')
                    ->simplePaginate(5);

        return new NotesCollection($notes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $img = $this->formatImage($request->image['body']);
        $img_url = $this->storeImage($img, $request->note['title'], $request->image['name']);

        $data = $request->note;
        $data['user_id'] = Auth::id();
        $data['img_url'] = $img_url;

        $this->note->create($data);

        return response('Ok',200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
        $notes = $this->note->where('user_id', $request->input('id'))->simplePaginate(15);

        return response($notes, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $this->note->find($request->input('id'))->delete();

        return response('Ok',200);
    }

    /**
     * @param $base64_img
     * @return false|Application|ResponseFactory|Response|string
     */
    private function formatImage($base64_img)
    {
        try {
            $img = substr($base64_img, strpos($base64_img, ',') + 1);
        }catch (Exception $e) {
            return $e->getMessage();
        }

        return base64_decode($img);
    }

    /**
     * @param $img
     * @param $title
     * @param $name
     * @return Application|ResponseFactory|Response|string
     */
    private function storeImage($img, $title, $name)
    {
        $img_url = 'notes/'.$title.'_'.$name;
        try {
            Storage::put($img_url, $img);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return $img_url;
    }
}
